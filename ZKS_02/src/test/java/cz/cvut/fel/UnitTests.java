package cz.cvut.fel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UnitTests {

    @Test
    void x1IsLessOrEqThen4() {
        int expectedResult = 5;
        int x1 = 4;
        int x2 = 0;
        int x3 = 1;
        Clazz myClazz = new Clazz();

        int result = myClazz.function1(x1, x2, x3);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void x1GreaterThen4AndSatisfySecondCondition() {
        int expectedResult = 20;
        int x1 = 5;
        int x2 = 3;
        int x3 = 7;
        Clazz myClazz = new Clazz();

        int result = myClazz.function1(x1, x2, x3);

        Assertions.assertEquals(expectedResult, result);

    }
}