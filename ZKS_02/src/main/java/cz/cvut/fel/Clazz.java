package cz.cvut.fel;

public class Clazz {
    public int function1(int x1, int x2, int x3) {
        int value;
        if (x1 > 4) {
            if (x2 < 5 && x3 < 10) {
                value = function2(x2, x3);
            } else {
                value = function3(x2, x3);
            }
            return value * 2;
        } else {
            value = function4(x1);
            return value;
        }
    }
    public int function2(int x1, int x2) {
        return x1 + x2;

    }
    public int function3(int x1, int x2) {
        return x1 - x2;
    }
    public int function4(int x1) {
        return x1 + 1;
    }
}
