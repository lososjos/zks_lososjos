package cz.cvut.fel;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Clazz clazzInstance = new Clazz();
        int output = clazzInstance.function1(5, 4, 9);
        System.out.println(output);
    }
}